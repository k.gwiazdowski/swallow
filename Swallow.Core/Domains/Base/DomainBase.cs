using System;

namespace Swallow.Core.Domains.Base
{
    public abstract class DomainBase<TId>
    {
        public TId Id { get; set; }
        public DateTime CreationDate { get; set; }
    }
}